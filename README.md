# BGRchain

This project exists to build a CI/CD pipeline to support Rchain development, dockerize Rchain, and test the docker image by deploying a 100 node testnet.

[Link to Project Origin Challenge](https://github.com/InsightDataScience/devops-challenges/blob/master/Mini-Challenges/Rchain-CICD.md)

This mini challenge builds off of the previous work done for the Rchain challenge by the Decentralized Consensus
Fellows during week 5 of the 2020C Insight program that work can be found at https://github.com/CheyenneAtapour/rchain or more neatly at the hyperlink below.

[Link to DC Rchain completed project](https://github.com/CheyenneAtapour/rchain)  
[Link to week 5 DC Challenge ExtendingRchain](https://github.com/InsightDataScience/decentralizedconsensus-challenges/blob/main/ExtendingRchain.md)

## Resources
- Check Rust syntax/compile
    - [Rahil link to medium rust ci/cd blog post](https://medium.com/astraol/optimizing-ci-cd-pipeline-for-rust-projects-gitlab-docker-98df64ae3bc4)
    - [Using cargo check to validate rust](https://doc.rust-lang.org/edition-guide/rust-2018/cargo-and-crates-io/cargo-check-for-faster-checking.html)
    - [Keat's validator](https://github.com/Keats/validator)
- Docker Stuff
    - [Gitlab's docker build documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
- ArgoCD
    - [Rahil link to ArgoCD setup](https://levelup.gitconnected.com/gitops-in-kubernetes-with-gitlab-ci-and-argocd-9e20b5d3b55b)

## Objectives
Met objectives will have a strike through mark drawn through them
- ~~Gitlab CI pipeline checks rust syntax and compiles~~
![alt text](images/FirstCompile.png)
- ~~Gitlab CI pipeline builds new docker image on successful commit~~
- Determine what is the order of operations, does argo create an EKS cluster? Do we need to set 
up our own eks cluster?:
    - Do argo tutorial w/ AWS focus maybe. 
    - Deploy EKS cluster
    - ArgoCD deploys image to Eks 
- Scale up EKS cluster to 100 node test net

